# Base Environment

Creating a base site development environment template using pyenv, poetry, pytest, pytest-cov, pre-commit, flake8, black and mypy. Either use this repo as template or DIY

## Use this repo as template

Clone to a new name using your IDE or use `git` on the command line:

With `ssh` set up:
```
git clone git@bitbucket.org:tymyrddin/base-environment.git yourchosenname
```

If you download the files and wish to rename the repo, find all instances of "base_environment" (``base_environment`` directory in root folder and ``test_base_environment.py`` in tests directory) and "base-environment" (repository name and name setting in ``pyproject.toml``) and rename.

Then install the dependencies with [poetry](https://bitbucket.org/tymyrddin/base-environment/wiki/Poetry.md).

## DIY

### Version control

- [Git](#git)

### IDE

- [Codium](https://bitbucket.org/tymyrddin/base-environment/wiki/Codium.md) or
- [VSCode](https://bitbucket.org/tymyrddin/base-environment/wiki/VSCode.md) or
- [Pycharm](https://bitbucket.org/tymyrddin/base-environment/wiki/Pycharm.md)

### Switching Python versions
For installing and switching between different versions of Python on a Linux machine:

- [Pyenv](https://bitbucket.org/tymyrddin/base-environment/wiki/Pyenv.md)

### Managing dependencies

- [Venv + pip](https://bitbucket.org/tymyrddin/base-environment/wiki/Venv+pip.md) or
- [Poetry](https://bitbucket.org/tymyrddin/base-environment/wiki/Poetry.md) or
- [Pipenv](https://bitbucket.org/tymyrddin/base-environment/wiki/Pipenv.md)

### Code quality
The "Usage looks like" sections in these pages can be used to test whether it is all working:
- [Pre-commit](https://bitbucket.org/tymyrddin/base-environment/wiki/Pre-commit.md)
- [Pytest](https://bitbucket.org/tymyrddin/base-environment/wiki/Pytest.md)
- [Black](https://bitbucket.org/tymyrddin/base-environment/wiki/Black.md)
- [Mypy](https://bitbucket.org/tymyrddin/base-environment/wiki/Mypy.md)
- [Isort](https://bitbucket.org/tymyrddin/base-environment/wiki/Isort.md)